package edu.ua.cs.cs200.lab9;

/**
 *
 * Paypal class provided by a third party
 *
 * @author Saheed
 *
 */
public class Paypal {

	/**
	 * Default constructor
	 */
	public Paypal() {
	}

	/**
	 * Pay amount via paypal
	 *
	 * @param amount
	 *            amount to be paid
	 */
	public void payViaPaypal(int amount) {
		System.out.println(amount + "$ is paid via PayPal!");
	}

}
